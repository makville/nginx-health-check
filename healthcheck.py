#!/usr/bin/env python3

import subprocess, sys
from shutil import copyfile

def run_cmd(cmd):
	process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = process.communicate()
	return str(out)

def check_health(server):
	parts = server.split(":")
	host = parts[0]
	if ';' in parts[1]:
		port = parts[1][0:-1]
	else:
		port = parts[1]
	health_output = run_cmd(["nmap", "-p", port, host])
	if "Host is up" in health_output:
		return True
	return False
	
	
#read the config
try:
	config_path = sys.argv[1] if len(sys.argv[1]) > 0 else '/etc/nginx/sites-available/default'
except:
	config_path = '/etc/nginx/sites-available/default'


copyfile(config_path, config_path + '.old')
config = '';
with open(config_path, 'r') as file:
	config = file.read()
	
lines = config.split("\n")

server_blocks = []
upstream_blocks = False
start = []
stop = []
counter = 0
server_lines = []
for line in lines:
	if "}" in line:
		upstream_blocks = False
		if len(stop) == len(start) - 1:
			stop.append(counter - 1)
			server_blocks.append(server_lines)
			server_lines = []
	if upstream_blocks == True:
		server_lines.append(line)
	if "upstream" in line:
		upstream_blocks = True
		start.append(counter + 1)
	counter += 1

checked_servers = []
servers = []
i = 0
j = 0
for blocks in server_blocks:
	j = 0
	for line in blocks:
		if "server" in line:
			if line[:1] == "#":
				line = line[1:]
			server = line.strip().split(' ')[1]
			if check_health(server):
				server_line = line
			else: 
				server_line = "#" + line
		server_blocks[i][j] = server_line
		j += 1		
	i +=1

#Recreate the config with checked servers
block = 0
new_lines = []
counter = 0
for i in range(len(start)):
	start_i = start[i]
	stop_i = stop[i]
	for line in lines[counter:start_i]:
		new_lines.append(line)
	for line in server_blocks[block]:
		new_lines.append(line)
	counter = stop_i + 1
	block += 1

for line in lines[counter:]:
	new_lines.append(line)	
	
config_file = open(config_path, "w")
config_file.write("\n".join(new_lines))
config_file.close()

#Ensure that we dont have a faulty config
test_output = run_cmd(["nginx", "-t"])
print(test_output)

if "test is successful" in test_output:
	try:
		subprocess.call("service nginx reload")
	except:
		exit(1)

